require 'rubygems'
require 'bundler/setup'

require 'rspec-puppet'

fixture_path = File.expand_path(File.join(__FILE__, '..', '..'))
puts "fixture_path = #{fixture_path}"

RSpec.configure do |c|
    c.module_path = File.join(fixture_path, 'modules')
    c.manifest_dir = File.join(fixture_path, 'spec','fixtures','manifests')
end
