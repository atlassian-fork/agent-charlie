require 'spec_helper'

describe 'svn' do

    default_facts = {
        :config_user => 'bob',
        :config_group => 'bobgroup',
        :config_charlie_home => '/Users/bob/atlassian/agent-charlie',
        :charlie_repository => 'http://www.test-repository.example.com',
        :config_tmp_directory => '/Users/bob/atlassian/tmp',
    }

    # Waiting for DRAKE-61
    context 'with operatingsystem => OSX' do
       let(:facts) {default_facts.merge({
           :operatingsystem             => 'Darwin',
           :macosx_productversion_major => '10.7',
       })}
       it {
            should contain_exec('install_subversion').with({
                :command => "/Users/bob/atlassian/agent-charlie/bin/brew-bottle subversion",
                :creates => "/usr/local/Cellar/subversion",
            })
       }
    end
    context 'with operatingsystem => Ubuntu' do
        let(:facts) {default_facts.merge({
            :operatingsystem             => 'ubuntu',
        })}
        it {
            should contain_package('subversion')
        }
    end

    context 'with operatingsystem => nothing' do
        let(:facts) {default_facts.merge({
            :operatingsystem             => 'WHAT???',
        })}
        it {
            expect {
                should contain_fail()
            }.to raise_error(Puppet::Error)
        }
    end

end
