class postgresql::ubuntu {
    package{'postgresql':
        ensure  => 'installed',
    }

    service {'postgresql_service':
        ensure  => 'running',
        enable  => true,
        name    => 'postgresql',
        require => Package['postgresql'],
    }
}
