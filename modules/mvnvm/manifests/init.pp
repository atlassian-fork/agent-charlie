class mvnvm {
    $tmp = $::config_tmp_directory
    $charlie_dir = $::config_charlie_home
    $repository = $::charlie_repository

    $user = $::config_user
    $group = $::config_group

    case $::operatingsystem {
        ubuntu: {
            exec {'install_mvnvm':
                command => 'curl -s https://bitbucket.org/mjensen/mvnvm/raw/master/mvn > $charlie_dir/bin/mvn',
                path    => ['/usr/local/bin','/usr/local/sbin','/usr/bin','/usr/sbin','/bin','/sbin']
            }
        }
        Darwin: {
            exec {'install_mvnvm':
                command => "${charlie_dir}/bin/brew install mvnvm",
                creates => '/usr/local/Cellar/mvnvm'
            }
        }
        default: {
            fail("Unsupported operating: ${::operatingsystem}")
        }
    }
}
