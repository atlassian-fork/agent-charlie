# This file simply figures out what configuration variables are required and calculates
# /whatever to different configuration things

# conf_files = [
#     'auto_config'
# ]

# module AgentCharlie::Config
#     extend self

#     @config_vars = Hash.new

#     def [](conf_name)
#         if @config_vars[conf_name].is_a? Proc
#             return @config_vars[conf_name].call
#         else
#             return @config_vars[conf_name]
#         end
#     end

#     def []=(conf_name, conf_value)
#         @config_vars[conf_name] = conf_value
#     end

#     def create(conf_name, &block)
#         @config_vars[conf_name] = block
#     end
# end

# # Pull in all configuration files
# Dir[File.join(File.expand_path('config', AgentCharlie.charlie_root), '*.rb')].each do |file|
#     require File.expand_path(file)
# end