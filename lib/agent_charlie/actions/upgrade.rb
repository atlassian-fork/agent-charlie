
require 'zlib'
require 'rubygems/package'
require 'open-uri'
require 'fileutils'

class AgentCharlie::Action::Upgrade

    def initialize(params = {})
        @branch = params[:branch]
        AgentCharlie::Logger.debug{"Initializing upgrade action with branch '#{@branch}'"}
    end

    def run_action(progress)
        @progress = progress
        charlie = AgentCharlie::Charlie.instance

        begin
            open("https://bitbucket.org/atlassian/agent-charlie/get/#{@branch}.tar.gz") do |io|

                z = Zlib::GzipReader.new(io)
                ungzip = StringIO.new(z.read)
                z.close

                tarreader = Gem::Package::TarReader.new(ungzip) do |tar|
                    tar.each do |f|
                        path_components = f.full_name.split(File::SEPARATOR)
                        stripped = path_components[1, path_components.length-1]
                        destination = File.join(AgentCharlie.charlie_root, stripped.join(File::SEPARATOR))
                        
                        if File.directory?(destination)
                            FileUtils.mkdir_p destination
                        else
                            dir = File.dirname(destination)
                            FileUtils.mkdir_p dir unless File.directory?(dir)
                            File.open(destination, 'wb') do |output|
                                output.print f.read
                            end
                        end

                        progress.increment(f.full_name)
                    end
                end
            end
        rescue => e
            AgentCharlie::Logger.error{"Failed to upgrade Agent Charlie: '#{$!}'"}
            AgentCharlie::Logger.error{$@.join("\n")}
            progress.failed("Could not upgrade Agent Charlie")
            return
        end

        # curl -o "$charlieTmp/charlie.tar.gz" "https://bitbucket.org/atlassian/agent-charlie/get/stable.tar.gz" &&
        # tar -C "$charlieRoot" -zxf "$charlieTmp/charlie.tar.gz" --strip-components 1 &&

        progress.complete("Upgrade was complete", nil)
    end

    def get_progress
        return @progress
    end

end