Facter.add('full_name') do |var|
    setcode do
        Facter::Util::Resolution.exec('finger -lp').split("\n")[0].split("Name: ", 2)[1]
    end
end