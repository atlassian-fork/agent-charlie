require 'puppet_helper/generic_login_singleton'

require 'open-uri'

if not Object.const_defined?('CrowdSingleton')
    class CrowdSingleton < GenericLoginSingleton

        def initialize
            super('Extranet-Crowd')
        end

        def authenticate
            begin
                open('https://maven.atlassian.com/private/',
                    :http_basic_authentication=>[@username,@password])
                return true
            rescue
                return false
            end
        end

        def information
            puts <<-'EOS'.gsub(/^\t*/, '')

			Agent Charlie requires your Extranet Crowd username and password so we can configure maven for you and
			install your ssh keys to stash.atlassian.com.

            EOS
        end

    end
end