require 'puppet_helper/crowd_singleton'
require 'puppet_helper/drop_permissions'

module Puppet::Parser::Functions
    newfunction(:crowd_password, :type => :rvalue, :arity => 0) do |args|
        begin
            password = Facter.config_crowd_password || ""
        rescue
        end

        if password == nil || password.length == 0
            DropPermissions::dropRoot do
                singleton = CrowdSingleton.instance
                password = singleton.password     
            end
        end

        password
    end
end
