require 'puppet_helper/bitbucket_singleton'
require 'puppet_helper/drop_permissions'

module Puppet::Parser::Functions
    newfunction(:bitbucket_password, :type => :rvalue, :arity => 0) do |args|
        begin
            password = Facter.config_bitbucket_password || ""
        rescue
        end

        if password == nil || password.length == 0
            DropPermissions::dropRoot do
                singleton = BitbucketSingleton.instance
                password = singleton.password
            end
        end

        password
    end
end
