require 'puppet_helper/crowd_singleton'
require 'puppet_helper/drop_permissions'

module Puppet::Parser::Functions
    newfunction(:crowd_username, :type => :rvalue, :arity => 0) do |args|
        begin
            username = Facter.config_crowd_username || ""
        rescue
        end

        if username == nil || username.length == 0
            DropPermissions::dropRoot do
                singleton = CrowdSingleton.instance
                username = singleton.username
            end
        end

        username
    end
end
