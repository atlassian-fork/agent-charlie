# name: Atlassian Checkout
# description: Checks out your favorite Atlassian products
# operatingsystems:
#   Darwin:
#     install-method:
#       method:         manual
#       manual-desc:    Installed to $ATLASSIAN_HOME/atlassian-checkout
#     dependencies:
#       - mavensettings
#   Ubuntu:
#     install-method:
#       method:         manual
#       manual-desc:    Installed to $ATLASSIAN_HOME/atlassian-checkout
#     dependencies:
#       - mavensettings

include atlassian-checkout
